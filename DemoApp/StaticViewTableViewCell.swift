//
//  StaticViewTableViewCell.swift
//  DemoApp
//
//  Created by Mindbowser on 05/02/19.
//  Copyright © 2019 MB. All rights reserved.
//

import UIKit

class StaticViewTableViewCell: UITableViewCell {

    @IBOutlet weak var mView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
