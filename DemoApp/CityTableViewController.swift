//
//  CityTableViewController.swift
//  DemoApp
//
//  Created by Mindbowser on 05/02/19.
//  Copyright © 2019 MB. All rights reserved.
//

import UIKit

class CityTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var mTableView: UITableView!
    var cityListArray: [String] = []
    var strCity = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mTableView.tableFooterView = UIView()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = mTableView.cellForRow(at: indexPath) as! MultipleCityTableViewCell
        
        if (textField == cell.cityTextField){
            print("move to cityVC: \(indexPath.row)")
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityListArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == cityListArray.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "StaticViewTableViewCell") as? StaticViewTableViewCell
            
            return cell!
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MultipleCityTableViewCell") as? MultipleCityTableViewCell
            if (indexPath.row < cityListArray.count){
                cell?.cityTextField.text = cityListArray[indexPath.row]
            }
            
            cell?.cancelButton.tag = indexPath.row
            cell!.cancelButton.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (cityListArray.count == 0){
            return 100
        }else{
            if(indexPath.row == cityListArray.count){
                return 100
            }else{
                return 44
            }
        }
    }
    
    /*
     //Delegate Methods
     func delegateCallback(str: String)  {
     if (cityListArray.count == 0){
     cityListArray.insert("\(cityListArray.count) - India", at: 0)
     }else{
     cityListArray.insert("\(cityListArray.count) - India", at: cityListArray.count)
     }
     self.mTableView.reloadData()
     }
     */
    
    @objc func buttonSelected(sender: UIButton){
        print("sender.tag: \(sender.tag)")
        cityListArray.remove(at: sender.tag)
        self.mTableView.reloadData()
    }
    
    @IBAction func actionOnBarButton(_ sender: Any) {
        self.ShowActionSheet()
        /*
        if (cityListArray.count == 0){
            cityListArray.insert("\(cityListArray.count) - India", at: 0)
        }else{
            cityListArray.insert("\(cityListArray.count) - India", at: cityListArray.count)
        }
        self.mTableView.reloadData()
        */
    }
    
    func addCityToArray(cityName: String)  {
        if (cityListArray.count == 0){
            cityListArray.insert("\(cityName)", at: 0)
        }else{
            cityListArray.insert("\(cityName)", at: cityListArray.count)
        }
        self.mTableView.reloadData()
    }
    
    func checkAndAdd(str: String)  {
        if (str != ""){
            if (self.cityListArray.count == 0){
                //check destinationCity from textfield and compare it with selected city at index 0
                self.addCityToArray(cityName: str)
            }else{
                print("self.cityListArray[self.cityListArray.count - 1]: \(self.cityListArray[self.cityListArray.count - 1])")
//                let lastItem = cityListArray.popLast()!
                let index = cityListArray.endIndex - 1
                let lastItem = cityListArray[index]
                print("lastItem: \(String(describing: lastItem))")
                if (lastItem == str){
                    print("please select the diffrent city?")
                }else{
                    self.addCityToArray(cityName: str)
                }
            }
        }
    }
    
    func ShowActionSheet() {
        let optionMenuController = UIAlertController(title: nil, message: "Choose City", preferredStyle: .actionSheet)
        
        let A = UIAlertAction(title: "ABC", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("ABC")
            self.strCity = "ABC"
            self.checkAndAdd(str: self.strCity)
        })
        let B = UIAlertAction(title: "DEF", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("DEF")
            self.strCity = "DEF"
            self.checkAndAdd(str: self.strCity)
        })
        let C = UIAlertAction(title: "GHI", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("GHI")
            self.strCity = "GHI"
            self.checkAndAdd(str: self.strCity)
        })
        let D = UIAlertAction(title: "XYZ", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("XYZ")
            self.strCity = "XYZ"
            self.checkAndAdd(str: self.strCity)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        optionMenuController.addAction(A)
        optionMenuController.addAction(B)
        optionMenuController.addAction(C)
        optionMenuController.addAction(D)
        optionMenuController.addAction(cancelAction)
        
        self.present(optionMenuController, animated: true, completion: nil)
    }
    
}
