//
//  ViewController.swift
//  DemoApp
//
//  Created by Mindbowser on 31/01/19.
//  Copyright © 2019 MB. All rights reserved.
//

import UIKit
import PaymentSDK

class ViewController: UIViewController, PGTransactionDelegate, UITextFieldDelegate {

    //MARK:- Variable Declarations
    
    @IBOutlet weak var dateTextField: UITextField!
      let datePicker = UIDatePicker()
    var txControllerVC = PGTransactionViewController()
    
    //MARK:- ViewController LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //MARK:- IBAction Methods
    
    @IBAction func actionOnPayButton(_ sender: Any) {
//        self.beginPayment()
//        self.showDatePicker()
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CityTableViewController") as? CityTableViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    //MARK:- Delegate Methods
    
    //this function triggers when transaction gets finished
    func didFinishedResponse(_ controller: PGTransactionViewController, response responseString: String) {
        print("responseString: \(responseString)")
        let msg : String = responseString
        var titlemsg : String = ""
        if let data = responseString.data(using: String.Encoding.utf8) {
            do {
                if let jsonresponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] , jsonresponse.count > 0{
                    titlemsg = jsonresponse["STATUS"] as? String ?? ""
                    print("titlemsg: \(titlemsg)")
                }
            } catch {
                print("Something went wrong")
            }
        }
        let actionSheetController: UIAlertController = UIAlertController(title: titlemsg , message: msg, preferredStyle: .alert)
        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel) {
            action -> Void in
            controller.navigationController?.popViewController(animated: true)
        }
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //this function triggers when transaction gets cancelled
    func didCancelTrasaction(_ controller : PGTransactionViewController) {
        print("didCancelTrasaction: \(controller.description)")
        controller.navigationController?.popViewController(animated: true)
    }
    
    //Called when a required parameter is missing.
    func errorMisssingParameter(_ controller : PGTransactionViewController, error : NSError?) {
        print("errorMisssingParameter: \(String(describing: error?.description))")
        controller.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Instance Methods
    
    func beginPayment() {
//        serv = serv.createProductionEnvironment()
        let type :ServerType = .eServerTypeStaging
        let order = PGOrder(orderID: "O100", customerID: "CI100", amount: "900", eMail: "kiran.jadhav@mindbowser.com", mobile: "7620368331")
        order.params = ["MID": "rxazcv89315285244163",
                        "ORDER_ID": "O100",
                        "CUST_ID": "CI100",
                        "MOBILE_NO": "7620368331",
                        "EMAIL": "kiran.jadhav@mindbowser.com",
                        "CHANNEL_ID": "WAP",
                        "WEBSITE": "WEBSTAGING",
                        "TXN_AMOUNT": "100.12",
                        "INDUSTRY_TYPE_ID": "Retail",
                        "CHECKSUMHASH": "oCDBVF+hvVb68JvzbKI40TOtcxlNjMdixi9FnRSh80Ub7XfjvgNr9NrfrOCPLmt65UhStCkrDnlYkclz1qE0uBMOrmuKLGlybuErulbLYSQ=",
                        "CALLBACK_URL": "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=order1"]
        self.txControllerVC =  (self.txControllerVC.initTransaction(for: order) as?PGTransactionViewController)!
        self.txControllerVC.title = "Paytm Payments"
        self.txControllerVC.setLoggingEnabled(true)
        if(type != ServerType.eServerTypeNone) {
            self.txControllerVC.serverType = type;
        } else {
            return
        }
        self.txControllerVC.merchant = PGMerchantConfiguration.defaultConfiguration()
        self.txControllerVC.delegate = self
        self.navigationController?.pushViewController(self.txControllerVC, animated: true)
    }
    
    //
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}

